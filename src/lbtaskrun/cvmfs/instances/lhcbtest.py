###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from os.path import normpath
from pathlib import Path

from lbtaskrun import app, cvmfs


@app.task(bind=True, name="lhcbtest_install_nightly")
def install_nightly(self, slot_name, project_name, build_id, platforms):
    return cvmfs.install_nightly(
        "lhcbdev.cern.ch",
        "/cvmfs/lhcbdev.cern.ch/nightlies",
        slot_name,
        project_name,
        build_id,
        platforms,
        restrict_lease=True,
    )


@app.task(bind=True, name="lhcbtest_deploy_lhcbdirac", priority=250)
def deploy_lhcbdirac(self):
    return cvmfs.deploy_lhcbdirac()


@app.task(bind=True, name="lhcbtest_cleanup_nightlies", priority=200)
def cleanup_nightlies(self):
    return cvmfs.cleanup_nightlies(
        "lhcbdev.cern.ch/nightlies", "/cvmfs/lhcbdev.cern.ch/nightlies"
    )


@app.task(bind=True, name="lhcbtest_clearlinks", priority=250)
def clearlinks(self):
    return cvmfs.cleanup_nightlies_links(
        "lhcbdev.cern.ch/nightlies", "/cvmfs/lhcbdev.cern.ch/nightlies"
    )


@app.task(bind=True, name="lhcbtest_lbinstall", priority=150)
def lbinstall(self, rpm_list):
    return cvmfs.cvmfs_lbinstall(
        "lhcbdev.cern.ch/lib",
        "/cvmfs/lhcbdev.cern.ch/lib",
        "/cvmfs/lhcb.cern.ch/lib",
        rpm_list,
    )


@app.task(bind=True, name="lhcbtest_rsync_certificates", priority=200)
def rsync_certificates(self):
    return cvmfs.rsync_certificates(
        "lhcbdev.cern.ch", "/cvmfs/lhcbdev.cern.ch/etc/grid-security"
    )


@app.task(
    bind=True,
    name="lhcbtest_deploy_conda_environment_v2",
    priority=220,
)
def deploy_conda_environment_v2(self, env_yaml, target_location, subdir):
    return cvmfs.deploy_conda_environment_v2(env_yaml, target_location, subdir)


@app.task(bind=True, name="lhcbtest_install_lhcbdirac", priority=150)
def install_lhcbdirac(self, version, diracos_version=None):
    return cvmfs.install_lhcbdirac(
        version,
        "lhcbdev.cern.ch/lhcbdirac",
        "/cvmfs/lhcbdev.cern.ch/lhcbdirac/",
        diracos_version=diracos_version,
    )


@app.task(bind=True, name="lhcbtest_lhcbdirac_set_prod_link", priority=210)
def lhcbdirac_set_prod_link(self, version):
    return cvmfs.lhcbdirac_set_prod_link(
        version, "lhcbdev.cern.ch/lhcbdirac", "/cvmfs/lhcbdev.cern.ch/lhcbdirac/"
    )


@app.task(
    bind=True,
    track_started=True,
    name="lhcbtest_install_nightly_conda_env",
    priority=190,
)
def install_nightly_conda_env(self, env_hash, env_spec, conda_subdir):
    return cvmfs.install_nightly_conda_env(
        "/cvmfs/lhcbdev.cern.ch/nightly-environments", env_hash, env_spec, conda_subdir
    )


@app.task(
    bind=True,
    track_started=True,
    name="lhcbtest_install_nightly_new",
    priority=200,
)
def install_nightly_new(self, flavour, slot, build_id, strip_prefix, url):
    prefix = Path("/cvmfs/lhcbdev.cern.ch/nightlies")
    prefix = prefix / flavour / slot / str(build_id)
    if strip_prefix:
        # Avoid directory traversal attacks
        strip_prefix_normed = normpath("/" + strip_prefix).lstrip("/")
        if strip_prefix_normed != strip_prefix:
            msg = f"Got {strip_prefix!r} but expected {strip_prefix_normed!r}"
            raise RuntimeError(msg)
        prefix = prefix / strip_prefix

    return cvmfs.new_nightlies.install_nightly_zip(
        prefix, url, strip_prefix=strip_prefix
    )

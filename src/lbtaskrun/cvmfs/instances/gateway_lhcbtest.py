###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from lbtaskrun import app, cvmfs


@app.task(bind=True, name="gateway_lhcbtest_gc", priority=200)
def gc(self):
    return cvmfs.cvmfs_garbage_collect(
        "lhcbdev.cern.ch", "http://gateway-cvmfs05.cern.ch:4929"
    )

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""

Module to interface with the CVMFS gateway.
It can get info about the current running transactions.

e.g. from the command line we can see:
$ curl -s http://gateway-cvmfs05.cern.ch:4929/api/v1/leases | jq
{
  "data": {
    "lhcbdev.cern.ch/test": {
      "key_id": "lhcbdev.cern.ch",
      "expires": "2020-03-24 22:32:34.793159112 +0100 CET"
    },
    "lhcbdev.cern.ch/titi": {
      "key_id": "lhcbdev.cern.ch",
      "expires": "2020-03-24 22:29:25.734463685 +0100 CET"
    }
  },
  "status": "ok"
}

"""

from __future__ import annotations

import time

import requests


def get_leases_url(gateway_url):
    return gateway_url + "/api/v1/leases"


def get_leases(gateway_url):
    response = requests.get(get_leases_url(gateway_url), timeout=1)
    content = response.json()
    if content["status"] != "ok":
        raise Exception("Error getting gateway lease: %s" % response.text)
    return content["data"]


def wait_until_transactions_done(gateway_url, poll_wait=29):
    while leases := get_leases(gateway_url):
        if len(leases) == 0:
            break
        time.sleep(poll_wait)  # poll again in 29 seconds

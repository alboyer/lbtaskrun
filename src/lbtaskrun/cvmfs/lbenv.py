###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module to deploy LbEnv on to CVMFS
"""

from __future__ import annotations

import logging
import os
import re
import shutil
import tempfile
from pathlib import Path

from git import Repo

from lbtaskrun.utils import logging_subprocess_run

logger = logging.getLogger(__name__)

DEFAULT_FLAVOURS = ["stable", "unstable"]
DEFAULT_PLATFORMS = ["linux-64", "linux-aarch64", "linux-ppc64le"]

BASH_TEMPLATE = r"""
export LBENV_PREFIX={prefix}
export PATH=${{LBENV_PREFIX}}/bin:$PATH

deactivate() {{
  PATH=$(
    echo "$PATH" | tr : \\0 | grep -vFzx "${{LBENV_PREFIX}}/bin" | tr \\0 : | sed 's/:\+$//'
  )
  export PATH
  unset LBENV_PREFIX
  unset -f deactivate
}}
""".strip()
CSH_TEMPLATE = r"""
setenv LBENV_PREFIX {prefix}
setenv PATH $LBENV_PREFIX/bin\:$PATH

alias deactivate 'setenv PATH `echo -n $PATH | tr : \\0 | grep -vFzx "${{LBENV_PREFIX}}/bin" | tr \\0 : | sed "s/:+$$//g"`; unsetenv LBENV_PREFIX; unalias deactivate'
""".strip()


def create_environments(install_root, *, flavours=None, platforms=None):
    install_root = Path(install_root)
    if flavours is None:
        flavours = DEFAULT_FLAVOURS
    if platforms is None:
        platforms = DEFAULT_PLATFORMS

    build_ids = [
        int(x.name) for x in install_root.iterdir() if re.fullmatch(r"[0-9]+", x.name)
    ]
    # Default to 2000 to distinguish from the old deployment method
    build_id = 2000
    if build_ids:
        build_id = max(max(build_ids) + 1, build_id)
    base_dir = install_root / str(build_id)
    base_dir.mkdir(parents=True)
    logger.info("Creating LbEnv installation in %s", base_dir)

    with tempfile.TemporaryDirectory() as tmpdir:
        # Clone lbenv-deployment
        repo_dir = Path(tmpdir) / "lbenv-deployment"
        Repo.clone_from(
            "https://gitlab.cern.ch/lhcb-core/lbenv-deployment.git",
            repo_dir,
        )

        # Prepare the sandbox container
        container_dir = Path(tmpdir) / "container"
        ret = logging_subprocess_run(
            [
                "apptainer",
                "build",
                "--fix-perms",
                "--sandbox",
                container_dir,
                "docker://centos:7",
            ]
        )
        ret.check_returncode()

        for flavour in flavours:
            yaml_fn = repo_dir / f"data/{flavour}-environment.yml"
            assert yaml_fn.exists()
            for subdir in platforms:
                rendered_yaml_fn = Path(tmpdir) / yaml_fn.name
                with rendered_yaml_fn.open("wt") as fp:
                    for line in yaml_fn.read_text().splitlines(keepends=True):
                        # Not yet available for non-x86_64 platforms
                        # https://conda-forge.org/status/#aarch64andppc64leaddition
                        # TODO: Create a way generic way of handling this,
                        # likely with "selectors" similar to conda constructor
                        if subdir != "linux-64" and "heaptrack" in line.lower():
                            continue
                        fp.write(line)

                _create_environment(
                    tmpdir,
                    container_dir,
                    rendered_yaml_fn,
                    base_dir / flavour,
                    subdir,
                )
                _apply_post_creation_patches(base_dir / flavour / subdir)
                _create_symlink(install_root, build_id, flavour, subdir)


def _create_environment(tmpdir, container_dir, yaml_fn, base_dir, subdir):
    base_dir.mkdir(parents=True, exist_ok=True)

    mamba_path = shutil.which("micromamba")
    if not mamba_path:
        msg = "Failed to find micromamba executable"
        raise RuntimeError(msg)

    workdir = Path(tmpdir) / "work"
    workdir.mkdir(exist_ok=True)

    with tempfile.TemporaryDirectory() as mamba_root:
        env_prefix = base_dir / subdir
        logger.info("Creating conda environment in %s", env_prefix)
        cmd = [
            "apptainer",
            "exec",
            f"--bind={mamba_path}:{mamba_path}:ro",
            f"--bind={mamba_root}:{mamba_root}",
            f"--env=MAMBA_ROOT_PREFIX={mamba_root}",
            f"--env=CONDA_SUBDIR={subdir}",
            f"--bind={yaml_fn}:{yaml_fn}:ro",
            f"--bind={base_dir}:{base_dir}",
            "--containall",
            f"--workdir={workdir}",
            container_dir,
            "bash",
            "-c",
            f"{mamba_path} create --quiet --yes --prefix={env_prefix} --file={yaml_fn}",
        ]
        ret = logging_subprocess_run(cmd)
        ret.check_returncode()


def _apply_post_creation_patches(prefix):
    # Create activation scripts
    bin_dir = prefix / "bin"
    (bin_dir / "activate").write_text(BASH_TEMPLATE.format(prefix=prefix))
    (bin_dir / "activate.csh").write_text(CSH_TEMPLATE.format(prefix=prefix))

    # Fix cloning with kerberos authentication
    # https://docs.gitlab.com/ee/integration/kerberos.html
    # http-basic-access-denied-when-cloning
    with (prefix / "etc" / "gitconfig").open("a") as fp:
        fp.write("\n[http]\n\temptyAuth = true")


def _create_symlink(install_root, build_id, flavour, host_os):
    link = "testing" if flavour == "stable" else flavour
    # Remove the link and creating a new one
    (install_root / link).mkdir(exist_ok=True, parents=True)
    dest = install_root / link / host_os
    src = f"../{build_id}/{flavour}/{host_os}"
    logging.info("Adding link %s -> %s", dest, src)
    if dest.exists():
        dest.unlink()
    os.symlink(src, dest)

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

__all__ = (
    "app",
    "ssl_cert_checking",
    "logging",
    "management",
    "__version__",
    "CvmfsAbortTransaction",
    "CvmfsTransaction",
)

import os
from importlib.metadata import PackageNotFoundError, version

if "LBTASKS_SENTRY_DSN" in os.environ:
    from . import sentry

    sentry.init()

from celery import Celery
from kombu import Queue

from . import config as default_config
from . import logging, ssl_cert_checking
from .cvmfs.transaction import CvmfsAbortTransaction, CvmfsTransaction

try:
    __version__ = version(__name__)
except PackageNotFoundError:  # pragma: no cover
    # package is not installed
    pass  # pragma: no cover


app = Celery("lbtaskrun", config_source=default_config)

from . import management  # NOQA Require app

# If a recoverable error is raise, we never want to stop retrying
app.Task.max_retries = None
# We tend run slow tasks so rate limit as short tasks are a sign something is wrong
# app.Task.rate_limit = "4/m"
# Other task related settings
app.Task.default_retry_delay = 3 * 60
app.conf.task_track_started = True
app.conf.task_default_priority = 110
# Here be dragons
app.conf.task_acks_late = False
app.conf.task_reject_on_worker_lost = False
app.conf.worker_lost_wait = 120

queue_arguments = {"x-max-priority": 255}
app.conf.task_queues = (
    Queue("default", routing_key="task.#", queue_arguments=queue_arguments),
    Queue("lhcbprod", routing_key="lhcbprod.#", queue_arguments=queue_arguments),
    Queue("lhcbdev", routing_key="lhcbdev.#", queue_arguments=queue_arguments),
    Queue(
        "gateway_lhcbtest",
        routing_key="gateway_lhcbtest.#",
        queue_arguments=queue_arguments,
    ),
    Queue("lhcbtest", routing_key="lhcbtest.#", queue_arguments=queue_arguments),
    Queue("lhcbcondb", routing_key="lhcbcondb.#", queue_arguments=queue_arguments),
)
app.conf.task_routes = {
    "task_*": {"queue": "default"},
    "lhcbdev_*": {"queue": "lhcbdev"},
    "lhcbprod_*": {"queue": "lhcbprod"},
    "gateway_lhcbtest_*": {"queue": "gateway_lhcbtest"},
    "lhcbtest_*": {"queue": "lhcbtest"},
    "lhcbcondb_*": {"queue": "lhcbcondb"},
}

app.conf.worker_pool_restarts = True
app.conf.worker_send_task_events = True
app.conf.task_send_sent_event = True

# Use CERN time to ensure beat jobs match the nightlies
app.conf.timezone = "Europe/Zurich"
app.conf.worker_prefetch_multiplier = 1

app.conf.result_extended = True
app.conf.result_expires = None

app.conf.event_serializer = "json"
app.conf.task_serializer = "json"
app.conf.task_compression = "gzip"
app.conf.result_serializer = "json"
app.conf.result_compression = "gzip"
app.conf.accept_content = ["json"]
app.conf.result_accept_content = ["json"]


# @after_task_publish.connect
# def update_sent_state(sender=None, headers=None, **kwargs):
#     # the task may not exist if sent using `send_task` which
#     # sends tasks by name, so fall back to the default result backend
#     # if that is the case.
#     task = current_app.tasks.get(sender)
#     backend = task.backend if task else current_app.backend
#     task_request = Context(
#         headers or {},
#         args=kwargs["body"][0],
#         called_directly=False,
#         kwargs=kwargs["body"][1],
#     )
#     # TODO: This doesn't work as expected
#     # backend.store_result(headers["id"], None, "PENDING", request=task_request)

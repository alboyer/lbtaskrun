###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os

import pytest

import lbtaskrun
from lbtaskrun.cvmfs.transaction import (
    DISABLE_TRANSACTION_FULLPATH,
    CvmfsLock,
    transactions_disabled,
)


class ExampleExceptionForTests(Exception):
    pass


def test_basic_transaction(monkeypatch, fake_cvmfs_canary):
    """
    Test setting the basic  CVMFS transaction attributes
    """
    # We need to mock subprocess.run to avoid the command being run
    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(*args)
            self.args = args[0]
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    # Change the environment
    monkeypatch.setattr("subprocess.run", SubprocessRunMock)
    monkeypatch.setattr("lbtaskrun.utils.logging_subprocess_run", SubprocessRunMock)

    class IsDirMock:
        def __init__(self, *args, **kwargs):
            print(f"isdir {args}")

    # Change the environment
    monkeypatch.setattr("os.path.isdir", IsDirMock)

    # Default case
    reponame = "lhcbdev.cern.ch"
    with lbtaskrun.CvmfsTransaction(reponame):
        assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
        assert subprocess_run_args[1] == [
            "cvmfs_server",
            "transaction",
            "lhcbdev.cern.ch",
        ]
    subprocess_run_args = []

    # enable retry
    with lbtaskrun.CvmfsTransaction(reponame, retry=True):
        assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
        assert subprocess_run_args[1][2] == "-r"
    subprocess_run_args = []

    # enable max retry count
    with lbtaskrun.CvmfsTransaction(reponame, retry=True, max_retry=10):
        assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
        assert subprocess_run_args[1] == [
            "cvmfs_server",
            "transaction",
            "-r",
            "-n",
            10,
            "lhcbdev.cern.ch",
        ]
    subprocess_run_args = []


def test_abort_transaction(monkeypatch, fake_cvmfs_canary):
    """
    Testing transaction abort
    """
    # We need to mock subprocess.run to avoid the command being run
    subprocess_run_args = []

    class SubprocessRunMock:
        def __init__(self, *args, **kwargs):
            subprocess_run_args.append(*args)
            print("===> f{subprocess_run_args}")
            self.args = args[0]
            self.returncode = 0
            self.stdout = b"stdout"
            self.stderr = b"stderr"

    # Change the environment
    monkeypatch.setattr("subprocess.run", SubprocessRunMock)
    monkeypatch.setattr("lbtaskrun.utils.logging_subprocess_run", SubprocessRunMock)

    class IsDirMock:
        def __init__(self, *args, **kwargs):
            print(f"isdir {args}")

    # Change the environment
    monkeypatch.setattr("os.path.isdir", IsDirMock)

    reponame = "lhcbdev.cern.ch"

    # Case when an unknown exception is thrown
    with pytest.raises(ExampleExceptionForTests):
        # Abort test
        with lbtaskrun.CvmfsTransaction(reponame):
            assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
            assert subprocess_run_args[1] == [
                "cvmfs_server",
                "transaction",
                "lhcbdev.cern.ch",
            ]
            msg = "Should not be handled"
            raise ExampleExceptionForTests(msg)
            # raise lbtaskrun.cvmfs.transaction.CvmfsAbortTransaction()

    assert subprocess_run_args[2] == [
        "cvmfs_server",
        "abort",
        "-f",
        "lhcbdev.cern.ch",
    ]
    subprocess_run_args = []

    # Checking that Abort was thrown
    with lbtaskrun.CvmfsTransaction(reponame):
        assert subprocess_run_args[0] == ["cvmfs_server", "tag", "-l"]
        assert subprocess_run_args[1] == [
            "cvmfs_server",
            "transaction",
            "lhcbdev.cern.ch",
        ]
        raise lbtaskrun.cvmfs.transaction.CvmfsAbortTransaction()
    assert subprocess_run_args[2] == [
        "cvmfs_server",
        "abort",
        "-f",
        "lhcbdev.cern.ch",
    ]
    subprocess_run_args = []


def test_lock(monkeypatch, tmp_path):
    """
    Test setting the lock file in the ${HOME}/var
    """

    os.environ["HOME"] = str(tmp_path)
    lockfilename = os.path.expandvars(DISABLE_TRANSACTION_FULLPATH)

    assert not transactions_disabled()

    lockreason = "test_lock"
    with CvmfsLock(reason=lockreason):
        assert transactions_disabled()
        assert os.path.exists(lockfilename)
        with open(lockfilename) as f:
            assert lockreason == f.read()

    assert not transactions_disabled()

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import os
from datetime import datetime
from os.path import join

import pytest

from lbtaskrun.cvmfs import clean_cvmfs_path

test_date = datetime.now()
day_name = datetime.strftime(test_date, "%a")
DEFAULT_SLOTS = ["slot1", "slot2"]
PROJECTS = ["project1", "project2"]
PLATFORMS = ["platform1", "platform2"]
CATALOGFILE = ".cvmfscatalog"


def create_bad_slot(path):
    os.mkdir(join(path, "1"))
    slotroot = join(path, "2")
    os.mkdir(slotroot)
    os.symlink("1", join(path, "Yesterday"))
    os.symlink("2", join(path, "Today"))
    os.symlink("2", join(path, "latest"))
    dayname = datetime.strftime(test_date, "%a")
    os.symlink("2", join(path, dayname))

    for p in PROJECTS:
        for pt in PLATFORMS:
            os.makedirs(join(slotroot, p, pt))

    # Create .cvmfscatalog as link, file or dir
    catfile = join(slotroot, PROJECTS[0], PLATFORMS[0], CATALOGFILE)
    catlink = join(slotroot, PROJECTS[0], PLATFORMS[1], CATALOGFILE)
    catdir = join(slotroot, PROJECTS[1], PLATFORMS[1], CATALOGFILE)

    with open(catfile, "w") as f:
        f.write("TEST")
    os.symlink(catfile, catlink)
    os.makedirs(catdir)


@pytest.fixture()
def fake_bad_nightly_slot(tmp_path):
    """Creates a fake nightlies slot directory"""
    create_bad_slot(tmp_path)
    return tmp_path


def test_clean_directory(fake_bad_nightly_slot):
    clean_cvmfs_path(fake_bad_nightly_slot)

    slotroot = join(fake_bad_nightly_slot, "2")
    catfile = join(slotroot, PROJECTS[0], PLATFORMS[0], CATALOGFILE)
    catlink = join(slotroot, PROJECTS[0], PLATFORMS[1], CATALOGFILE)
    catdir = join(slotroot, PROJECTS[1], PLATFORMS[1], CATALOGFILE)

    assert not os.path.exists(catfile)
    assert not os.path.exists(catlink)
    assert not os.path.exists(catdir)


def test_clean_directory_keep_normalfile(fake_bad_nightly_slot):
    clean_cvmfs_path(fake_bad_nightly_slot, remove_catalog_normalfiles=False)

    slotroot = join(fake_bad_nightly_slot, "2")
    catfile = join(slotroot, PROJECTS[0], PLATFORMS[0], CATALOGFILE)
    catlink = join(slotroot, PROJECTS[0], PLATFORMS[1], CATALOGFILE)
    catdir = join(slotroot, PROJECTS[1], PLATFORMS[1], CATALOGFILE)

    assert os.path.exists(catfile)
    assert not os.path.exists(catlink)
    assert not os.path.exists(catdir)

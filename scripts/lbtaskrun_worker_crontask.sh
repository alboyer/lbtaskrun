#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
source "${HOME}/.bashrc"

# Use bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

export CELERY_PID_FILE="${SW_LOCATION}/celery_worker.pid"

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

function start_celery() {
    # Conda's activation scripts can't be used with set -u
    set +u
    source "${SW_LOCATION}/activate_latest" lbtaskrun-env
    set -u
    celery -A "lbtaskrun.cvmfs.instances.${LBTASKRUN_QUEUE}" worker \
        --concurrency=1 --loglevel=info -Q "${LBTASKRUN_QUEUE}" \
        --pidfile="${CELERY_PID_FILE}" -n "${LBTASKRUN_QUEUE}.$(hostname)" 2>&1 | \
        multilog s5000000000 n1000 ws.zst "!${THIS_SCRIPT_DIR}/log_compressor.sh" "${HOME}/logs-new/celery_worker.$(hostname)"
}

if [ -f  "${CELERY_PID_FILE}" ]; then
  kill -0 "$(cat "${CELERY_PID_FILE}")" || start_celery
else
 start_celery
fi
